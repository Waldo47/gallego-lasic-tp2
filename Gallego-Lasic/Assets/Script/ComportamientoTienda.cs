﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ComportamientoTienda : MonoBehaviour
{
    Dictionary<string, int> tienda = new Dictionary<string, int>();
    ItemTienda item = new ItemTienda();
    public string GenerarCodigo()
    {
       var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        var pos=0;
       System.Random random = new System.Random();
       var finalString = "";
       for (int i = 0; i < 8; i++) 
       {
            pos = random.Next(0, chars.Length);
            finalString += chars[pos];
       }
        return finalString;
    }

    public int GenerarPrecio()
    {
        int precio = 0;
        System.Random rnd = new System.Random();
        precio = rnd.Next(1, 10) * 100;
        return precio;
    }
    public class ItemTienda
    {
        private int costo;

        public int Costo
        {
            get { return costo; }
            set { costo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

    }

    void Start()
    {
        for (int i = 0; i < 10000; i++)
        {
            item.Nombre = GenerarCodigo();
            item.Costo = GenerarPrecio();
            while (tienda.ContainsKey(item.Nombre)==false)
            {
                tienda.Add(item.Nombre, item.Costo);
            }
            Debug.Log(item.Nombre + "------" + item.Costo);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
