﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class ComportamientoPelea : MonoBehaviour
{

    Personaje p1 = new Personaje();
    Personaje p2 = new Personaje();
    int aux =0;

    public class Arma
    {
        private int capacidadDeAtaque;

        public int CapacidadDeAtaque
        {
            get { return capacidadDeAtaque; }
            set { capacidadDeAtaque = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

    }

    public class Personaje
    {
        private Arma arma = new Arma();

        public Arma Arma
        {
            get { return arma; }
            set { arma = value; }
        }


        private float vida;

        public float Vida
        {
            get { return vida; }
            set { vida = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int fuerza;

        public int Fuerza
        {
            get { return fuerza; }
            set { fuerza = value; }
        }

        private int defensa;

        public int Defensa
        {
            get { return defensa; }
            set { defensa = value; }
        }

        public void Pegar(Personaje personaje)
        {
            personaje.Vida -= (this.Fuerza * Arma.CapacidadDeAtaque)/personaje.defensa;
        }

    }
    void Start()
    {
        p1.Nombre = "Ken";
        p2.Nombre = "Ryu";
        p1.Vida = 1000;
        p1.Fuerza = 10;
        p1.Defensa = 5;
        p2.Vida = 1000;
        p2.Fuerza = 5;
        p2.Defensa = 10;
        p1.Arma.CapacidadDeAtaque = 30;
        p1.Arma.Nombre = "Devorador de cabezas";
        p2.Arma.CapacidadDeAtaque = 35;
        p2.Arma.Nombre = "Un Ultimo combate";
    }

    // Update is called once per frame
    void Update()
    {
        if (p1.Vida <= 0 && aux ==0)
        {
            Debug.Log(p1.Nombre + " ha sido asesinado por " + p2.Nombre);
            aux++;
        }
        else
        {
            if(p2.Vida <=0 && aux == 0)
            {
                Debug.Log(p2.Nombre + " ha sido asesinado por " + p1.Nombre);
                aux++;
            }
        }
        if (aux != 1)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Debug.Log("Ken le pega a Ryu");
                p1.Pegar(p2);
                Debug.Log("Ryu tiene " + p2.Vida);
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log("Ryu le pega a Ken");
                p2.Pegar(p1);
                Debug.Log("Ken tiene " + p1.Vida);
            }
        }
    }
}
